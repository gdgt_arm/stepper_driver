/*
  Copyright (c) 2014-2016 Arduino LLC.  All right reserved.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  See the GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#ifndef _VARIANT_UW_STEPPER_DRIVER_
#define _VARIANT_UW_STEPPER_DRIVER_

// The definitions here needs a SAMD core >=1.6.10
#define ARDUINO_SAMD_VARIANT_COMPLIANCE 10610

#include <WVariant.h>

/*----------------------------------------------------------------------------
 *        Definitions
 *----------------------------------------------------------------------------*/

// Frequency of the board main oscillator
#define VARIANT_MAINOSC (32768ul)

// Master clock frequency
#define VARIANT_MCK     (48000000ul)

/*----------------------------------------------------------------------------
 *        Pins
 *----------------------------------------------------------------------------*/

// Number of pins defined in PinDescription array
#ifdef __cplusplus
extern "C" unsigned int PINCOUNT_fn();
#endif
#define PINS_COUNT           (PINCOUNT_fn())
#define NUM_DIGITAL_PINS     (34u)
#define NUM_ANALOG_INPUTS    (0u)
#define NUM_ANALOG_OUTPUTS   (0u)

// Low-level pin register query macros
#define digitalPinToPort(P)        ( &(PORT->Group[g_APinDescription[P].ulPort]) )
#define digitalPinToBitMask(P)     ( 1 << g_APinDescription[P].ulPin )
#define portOutputRegister(port)   ( &(port->OUT.reg) )
#define portInputRegister(port)    ( &(port->IN.reg) )
#define portModeRegister(port)     ( &(port->DIR.reg) )
#define digitalPinHasPWM(P)        ( g_APinDescription[P].ulPWMChannel != NOT_ON_PWM || g_APinDescription[P].ulTCChannel != NOT_ON_TIMER )

/*
 * digitalPinToTimer(..) is AVR-specific and is not defined for SAMD
 * architecture. If you need to check if a pin supports PWM you must
 * use digitalPinHasPWM(..).
 *
 * https://github.com/arduino/Arduino/issues/1833
 */
// #define digitalPinToTimer(P)

/*
 * Stepper Driver Pins
 */
#define PIN_DIR_1           (0ul)
#define PIN_STEP_1          (1ul)
#define PIN_DIAG_1          (2ul)
#define PIN_EN_1            (3ul)

#define PIN_DIR_2           (4ul)
#define PIN_STEP_2          (5ul)
#define PIN_DIAG_2          (6ul)
#define PIN_EN_2            (7ul)

#define PIN_DIR_3           (8ul)
#define PIN_STEP_3          (9ul)
#define PIN_DIAG_3          (10ul)
#define PIN_EN_3            (11ul)

#define PIN_DIR_4           (12ul)
#define PIN_STEP_4          (13ul)
#define PIN_DIAG_4          (14ul)
#define PIN_EN_4            (15ul)

#define PIN_DIR_5           (16ul)
#define PIN_STEP_5          (17ul)
#define PIN_DIAG_5          (18ul)
#define PIN_EN_5            (19ul)

#define PIN_DIR_6           (20ul)
#define PIN_STEP_6          (21ul)
#define PIN_DIAG_6          (22ul)
#define PIN_EN_6            (23ul)

/*
 * LED/NEO
 */
#define PIN_LED              (32u)
#define LED_BUILTIN          PIN_LED

#define PIN_NEO              (33u)

/*
 * Analog pins
 */
#define PIN_A0 (0u)
static const uint8_t A0  = PIN_A0;
#define ADC_RESOLUTION 12

/*
 * SPI Interfaces
 */
#define SPI_INTERFACES_COUNT 0

/*
 * Wire Interfaces
 */
#define WIRE_INTERFACES_COUNT 0

/*
 * USB
 */
#define PIN_USB_DM          (30ul)
#define PIN_USB_DP          (31ul)

/*
 * I2S Interfaces
 */
#define I2S_INTERFACES_COUNT 0

/*
 * Serial interfaces
 */
#ifdef __cplusplus
#include "SERCOM.h"
#include "Uart.h"

// Instances of SERCOM
extern SERCOM sercom0;  // serial1
extern SERCOM sercom1;  // serial2
extern SERCOM sercom2;
extern SERCOM sercom3;
extern SERCOM sercom4;
extern SERCOM sercom5;  // serial

// Serial - SERCOM5alt
extern Uart Serial;
#define PIN_SERIAL_TX       (24ul)
#define PIN_SERIAL_RX       (25ul)
#define PAD_SERIAL_TX       (UART_TX_PAD_2)
#define PAD_SERIAL_RX       (SERCOM_RX_PAD_3)

// Serial1 - SERCOM0
extern Uart Serial1;
#define PIN_SERIAL1_TX       (26ul)
#define PIN_SERIAL1_RX       (27ul)
#define PAD_SERIAL1_TX       (UART_TX_PAD_2)
#define PAD_SERIAL1_RX       (SERCOM_RX_PAD_3)

// Serial2 - SERCOM1
extern Uart Serial2;
#define PIN_SERIAL2_TX       (28ul)
#define PIN_SERIAL2_RX       (29ul)
#define PAD_SERIAL2_TX       (UART_TX_PAD_2)
#define PAD_SERIAL2_RX       (SERCOM_RX_PAD_3)

#endif // __cplusplus

#ifdef __cplusplus
extern "C" {
#endif
unsigned int PINCOUNT_fn();
#ifdef __cplusplus
}
#endif

// These serial port names are intended to allow libraries and architecture-neutral
// sketches to automatically default to the correct port name for a particular type
// of use.  For example, a GPS module would normally connect to SERIAL_PORT_HARDWARE_OPEN,
// the first hardware serial port whose RX/TX pins are not dedicated to another use.
//
// SERIAL_PORT_MONITOR        Port which normally prints to the Arduino Serial Monitor
//
// SERIAL_PORT_USBVIRTUAL     Port which is USB virtual serial
//
// SERIAL_PORT_LINUXBRIDGE    Port which connects to a Linux system via Bridge library
//
// SERIAL_PORT_HARDWARE       Hardware serial port, physical RX & TX pins.
//
// SERIAL_PORT_HARDWARE_OPEN  Hardware serial ports which are open for use.  Their RX & TX
//                            pins are NOT connected to anything by default.
#define SERIAL_PORT_USBVIRTUAL      SerialUSB
#define SERIAL_PORT_MONITOR         Serial
// Serial has no physical pins broken out, so it's not listed as HARDWARE port
// #define SERIAL_PORT_HARDWARE        Serial
// #define SERIAL_PORT_HARDWARE_OPEN   Serial

#endif /* _VARIANT_UW_STEPPER_DRIVER_ */

