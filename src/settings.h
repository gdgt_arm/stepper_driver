#ifndef SETTINGS_H
#define SETTINGS_H

#include <Arduino.h>

#include "main.h"

/********************************************************
   Function Prototypes
********************************************************/

uint8_t loadSettings();
void updateSetting(uint8_t motor, uint8_t idx, float v);
void saveSettings();
void dumpSettings(uint8_t sets = 'B');
void defaultSettings();

#define NUM_OF_DOF 5
#define NUM_OF_MOTORS 5

extern struct JOINTCONSTS kJoint[NUM_OF_DOF];
extern struct MOTORCONSTS kMotor[NUM_OF_MOTORS];
extern const char jointNames[NUM_OF_DOF];

#endif
