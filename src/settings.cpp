#include "settings.h"

#include <FlashStorage_SAMD.h>

//#define EEPROM_EMULATION_SIZE     (4 * 1024)
#define FLASH_DEBUG 0 // Use 0-2. Larger for more debugging messages

uint8_t loadSettings()
{
    int eeAddress = 0;
    if (EEPROM.read(eeAddress) == 0xff) {
        defaultSettings();
        saveSettings();
    }
    for (byte i = 0; i < NUM_OF_DOF; i++) {
        EEPROM.get(eeAddress, kJoint[i]);
        eeAddress += sizeof(struct JOINTCONSTS);
    }
    for (byte i = 0; i < NUM_OF_MOTORS; i++) {
        EEPROM.get(eeAddress, kMotor[i]);
        eeAddress += sizeof(struct MOTORCONSTS);
    }
    serprintln(F("Constants read from EEPROM"));
    dumpSettings();

    return 0;
}

void updateSetting(uint8_t dof, uint8_t idx, float v)
{
    SerialUSB.print(jointNames[dof]);
    SerialUSB.print(' ');
    switch (idx) {
    case 0:
        kJoint[dof].degMin = v;
        SerialUSB.print(F("degMin"));
        break;
    case 1:
        kJoint[dof].degMax = v;
        SerialUSB.print(F("degMax"));
        break;
    case 2:
        kJoint[dof].degPark = v;
        SerialUSB.print(F("degPark"));
        break;
    case 3:
        kJoint[dof].degUnpark = v;
        SerialUSB.print(F("degUnpark"));
        break;
    case 4:
        kJoint[dof].degExtend = v;
        SerialUSB.print(F("degExtend"));
        break;
    case 5:
        kJoint[dof].fStepsPerDeg = v;
        SerialUSB.print(F("fStepsPerDeg"));
        break;
    case 6:
        kJoint[dof].comp = v;
        SerialUSB.print(F("comp"));
        break;
    }
    SerialUSB.print(F(" set to "));
    SerialUSB.println(v, 4);
}

void saveSettings()
{
    int eeAddress = 0;
    for (byte i = 0; i < NUM_OF_DOF; i++) {
        EEPROM.put(eeAddress, kJoint[i]);
        eeAddress += sizeof(struct JOINTCONSTS);
    }
    for (byte i = 0; i < NUM_OF_MOTORS; i++) {
        EEPROM.put(eeAddress, kMotor[i]);
        eeAddress += sizeof(struct MOTORCONSTS);
    }
    EEPROM.commit();
    serprintln(F("Constants saved to EEPROM"));
}

void dumpFloat(float f, uint8_t dp, uint8_t cp)
{
    uint8_t d;
    for (d = 1; d < 10; d++) {
        if (pow(10, d) > abs(f))
            break;
    }
    uint8_t spaces = cp - dp - d - 1 - (f < 0 ? 1 : 0);
    while (spaces--)
        SerialUSB.print(' ');
    SerialUSB.print(f, dp);
}

void dumpSettings(uint8_t sets)
{
    if (sets == 'B' || sets == 'J') {
        SerialUSB.println(F("J  0 degMin  1 degMax 2 degPark  3 degUnp  4 degExt 5 stepsPerDeg    6 comp"));
        for (uint8_t i = 0; i < NUM_OF_DOF; i++) {
            SerialUSB.print(jointNames[i]);
            dumpFloat(kJoint[i].degMin, 3, 10);
            dumpFloat(kJoint[i].degMax, 3, 10);
            dumpFloat(kJoint[i].degPark, 3, 10);
            dumpFloat(kJoint[i].degUnpark, 3, 10);
            dumpFloat(kJoint[i].degExtend, 3, 10);
            dumpFloat(kJoint[i].fStepsPerDeg, 3, 14);
            dumpFloat(kJoint[i].comp, 3, 10);
            SerialUSB.println();
        }
        SerialUSB.println();
    }
    if (sets == 'B' || sets == 'M') {
        SerialUSB.println(F("M microSteps currentMax currentHold maxSpeed"));
        for (uint8_t i = 0; i < NUM_OF_MOTORS; i++) {
            char buffer[80];
            sprintf(buffer, "%d %10d %10d %11d %8d", i, kMotor[i].microSteps, kMotor[i].currentMax, kMotor[i].currentHold, kMotor[i].maxSpeed);
            SerialUSB.println(buffer);
        }
        SerialUSB.println();
    }
}

void defaultSettings()
{
    // S shoulder
    kJoint[JOINT_SHOULDER] = {
        75.0, // degMin °
        189.0, // degMax °
        189.0, // degPark °
        171.2, // degUnpark °
        81.2, // degExtend °
        11.15, // stepsPerDeg
        0.0 // comp
    };

    kMotor[MOTOR_SHOULDER] = {
        2, // microSteps
        2000, // currentMax milliamps
        1000, // currentHold milliamps
        750, // maxSpeed
        1500 // accel
    };

    // E elbow
    kJoint[JOINT_ELBOW] = {
        18.4, // degMin °
        189.6, // degMax °
        30.0, // 18.4, // degPark °
        45.0, // 30.0, // degUnpark °
        189.6, // degExtend °
        11.05, // stepsPerDeg
        0.280 // comp - shoulder effect on elbow
    };

    kMotor[MOTOR_ELBOW] = {
        2, // microSteps
        2000, // currentMax milliamps
        1000, // currentHold milliamps
        1000, // maxSpeed
        1500 // accel
    };

    // W wrist
    kJoint[JOINT_WRIST] = {
        -105.0, // degMin °
        85.0, // degMax °
        -37.0, // 62.6, // degPark °
        -10.0, // 30.0, // degUnpark ° 68.8 = horiz
        0.0, // degExtend °
        9.99101, // stepsPerDeg
        0.300 // comp - elbow effect on wrist
    };

    kMotor[MOTOR_WRIST0] = {
        2, // microSteps
        2000, // currentMax milliamps
        1000, // currentHold milliamps
        1000, // maxSpeed
        1500 // accel
    };

    kMotor[MOTOR_WRIST1] = {
        2, // microSteps
        2000, // currentMax milliamps
        1000, // currentHold milliamps
        1000, // maxSpeed
        1500 // accel
    };

    // R rotate
    kJoint[JOINT_ROTATE] = {
        -720.0, // degMin °
        720.0, // degMax °
        0.0, // degPark °
        0.0, // degUnpark °
        0.0, // degExtend °
        9.02024, // stepsPerDeg
        0.0 // comp
    };

    // P pivot
    kJoint[JOINT_PIVOT] = {
        -12.0, // degMin °
        12.0, // degMax °
        0.0, // degPark °
        0.0, // degUnpark °
        0.0, // degExtend °
        47.54063, // stepsPerDeg
        0.0 // comp
    };

    kMotor[MOTOR_PIVOT] = {
        2, // microSteps
        2000, // currentMax milliamps
        1000, // currentHold milliamps
        1000, // maxSpeed
        750 // accel
    };
    serprintln(F("Constants set to default values"));
}
