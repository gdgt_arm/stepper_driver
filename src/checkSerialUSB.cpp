#include "main.h"
#include <Arduino.h>

extern DelayTimer dtBlink;
extern float fTargetDegrees[NUM_OF_DOF];
extern Adafruit_NeoPixel strip;
extern uint8_t neoColor[3];
extern const char jointNames[NUM_OF_DOF];
extern uint8_t velocityFactor;
extern AccelStepper accelSteppers[NUM_OF_DOF];
extern int8_t lastMotionQueueIndex;
extern struct MOTIONTARGET motionQueue[MOTION_QUEUE_SIZE];

const char* moveToErrors[8] = {
    "none",
    "not in front",
    "too far away",
    "P angle out of range",
    "S angle out of range",
    "E angle out of range",
    "W angle out of range",
    "motion queue full"
};

uint8_t serialInputState = SERIAL_STATE_IDLE;
uint8_t dofIndex = 0;
uint8_t constIndex;
int8_t jogDir;
char rxBuf[10];
uint8_t rxBufIndex;

#define MOTION_MODE_ANGLES 0
#define MOTION_MODE_MOVETO 1

uint8_t lastMotionMode;

int16_t moveToParams[5] = { 0, 0, 0, 0, 90 };

void checkSerialUSB()
{
    if (SerialUSB.available() > 0) {
        LED_ON();
        dtBlink.reset(0, 10);
        char rc = SerialUSB.read();
        if (rc >= 'a' && rc <= 'z')
            rc -= 32; // Convert a-z to A-Z
        SerialUSB.print(rc);
        uint8_t ri = (rc >= '0' && rc <= '9') ? rc - 48 : 0xff;
        switch (serialInputState) {
        case SERIAL_STATE_IDLE:
            switch (rc) {
            case 'C':
                SerialUSB.print("onstant ");
                serialInputState = SERIAL_STATE_K_M_ID;
                break;
            case 'J':
                SerialUSB.print("oint ");
                serialInputState = SERIAL_STATE_JOINT_NUM;
                lastMotionMode = MOTION_MODE_ANGLES;
                break;
            case 'O':
                SerialUSB.print(" jog ");
                serialInputState = SERIAL_STATE_JOG_NUM;
                break;
            case 'M':
                SerialUSB.print("oveTo ");
                serialInputState = SERIAL_STATE_MOVETO_PARAM;
                lastMotionMode = MOTION_MODE_MOVETO;
                break;
            case 'V':
                SerialUSB.print("elocity factor ");
                serialInputState = SERIAL_STATE_VEL_FACTOR;
                break;
            case 'G':
                SerialUSB.println("o");
                if (lastMotionMode == MOTION_MODE_ANGLES)
                    handleCmd(ARM_CMD_MOVE);
                else if (lastMotionMode == MOTION_MODE_MOVETO) {
                    uint8_t err = moveTo(moveToParams[0], moveToParams[1], moveToParams[2], moveToParams[3], moveToParams[4], velocityFactor);
                    vardump("error", err);
                    SerialUSB.println(moveToErrors[err]);
                }
                break;
            case ' ':
                SerialUSB.println("STOP");
                handleCmd(ARM_CMD_STOP);
                break;
            case 'L':
                SerialUSB.println("imp");
                handleCmd(ARM_CMD_LIMP);
                break;
            case 'I':
                SerialUSB.println("nit");
                handleCmd(ARM_CMD_INIT);
                break;
            case 'P':
                SerialUSB.println("ark");
                handleCmd(ARM_CMD_PARK);
                break;
            case 'U':
                SerialUSB.println("npark");
                handleCmd(ARM_CMD_UNPARK);
                break;
            case 'E':
                SerialUSB.println("xtend");
                handleCmd(ARM_CMD_EXTEND);
                break;
            case 'D':
                SerialUSB.println("ance");
                handleCmd(ARM_CMD_DANCE);
                break;
            case '7':
                SerialUSB.println(" tool ready");
                handleCmd(ARM_CMD_TOOL_READY);
                break;
            case '8':
                SerialUSB.println(" tool get");
                handleCmd(ARM_CMD_TOOL_GET);
                break;
            // case '9':
            //     SerialUSB.println(" tool withdraw");
            //     handleCmd(ARM_CMD_TOOL_LOCK);
            //     break;
            // case '0':
            //     SerialUSB.println(" tool return");
            //     handleCmd(ARM_CMD_TOOL_RETURN);
            //     break;
            case 'N':
                SerialUSB.print("EO ");
                serialInputState = SERIAL_STATE_NEO_COLOR;
                break;
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
                // jogMotor(ri, jogDir);
                dofIndex = ri;
                SerialUSB.print(' ');
                SerialUSB.println(jointNames[ri]);
                break;
            case '.':
                // jogDir = 1;
                // SerialUSB.println(" Jog dir = +1");
                jogMotor(dofIndex, 1);
                break;
            case ',':
                // jogDir = -1;
                // SerialUSB.println(" Jog dir = -1");
                jogMotor(dofIndex, -1);
                break;
            case 'W':
                SerialUSB.println("here");
                where();
                break;
            case '?':
            case '/':
                SerialUSB.println();
                SerialUSB.println("c = constants");
                SerialUSB.println("    s = shoulder");
                SerialUSB.println("    e = elbow");
                SerialUSB.println("    w = wrist");
                SerialUSB.println("    r = rotate");
                SerialUSB.println("    p = pivot");
                SerialUSB.println("    j = dump joints");
                SerialUSB.println("    m = dump motors");
                SerialUSB.println("    ^d = default");
                SerialUSB.println("    ^s = save");
                SerialUSB.println("    ^l = load");
                SerialUSB.println("j = joint");
                SerialUSB.println("    s = shoulder");
                SerialUSB.println("    e = elbow");
                SerialUSB.println("    w = wrist");
                SerialUSB.println("    r = rotate");
                SerialUSB.println("    p = pivot");
                SerialUSB.println("o = jog");
                SerialUSB.println("    s = shoulder");
                SerialUSB.println("    e = elbow");
                SerialUSB.println("    w = wrist");
                SerialUSB.println("    r = rotate");
                SerialUSB.println("    p = pivot");
                SerialUSB.println("m = move to");
                SerialUSB.println("    x, y, z = mm");
                SerialUSB.println("    r = rotate angle");
                SerialUSB.println("    g = gripper angle");
                SerialUSB.println("v = velocity");
                SerialUSB.println("g = go");
                SerialUSB.println("<space> = stop");
                SerialUSB.println("l = limp");
                SerialUSB.println("i = init");
                SerialUSB.println("p = park");
                SerialUSB.println("u = unpark");
                SerialUSB.println("e = extend");
                SerialUSB.println("w = where");
                // SerialUSB.println("2 = 2-finger");
                // SerialUSB.println("3 = 3-finger");
                // SerialUSB.println("o = open");
                // SerialUSB.println("c = close");
                SerialUSB.println("n = neo");
                SerialUSB.println("    r = red");
                SerialUSB.println("    b = blue");
                SerialUSB.println("    g = green");
                SerialUSB.println("1-5 = jog motor");
                SerialUSB.println(". = jog dir +1");
                SerialUSB.println(", = jog dir -1");
                SerialUSB.println();
                break;
            case '\n':
            case '\r':
                SerialUSB.println("Ready");
                break;
            default:
                SerialUSB.println(" INVALID");
            }
            constIndex = 0;
            rxBufIndex = 0;
            break;
        case SERIAL_STATE_JOINT_NUM:
        case SERIAL_STATE_JOG_NUM:
            switch (rc) {
            case 'S':
                dofIndex = 0;
                SerialUSB.print("houlder ");
                break;
            case 'E':
                dofIndex = 1;
                SerialUSB.print("lbow ");
                break;
            case 'W':
                dofIndex = 2;
                SerialUSB.print("rist ");
                break;
            case 'R':
                dofIndex = 3;
                SerialUSB.print("otate ");
                break;
            case 'P':
                dofIndex = 4;
                SerialUSB.print("ivot ");
                break;
            default:
                SerialUSB.println(" Invalid joint");
                serialInputState = SERIAL_STATE_IDLE;
            }
            if (serialInputState == SERIAL_STATE_JOINT_NUM)
                serialInputState = SERIAL_STATE_JOINT_ANGLE;
            else if (serialInputState == SERIAL_STATE_JOG_NUM)
                serialInputState = SERIAL_STATE_JOG_ANGLE;
            break;
        case SERIAL_STATE_MOVETO_PARAM:
            switch (rc) {
            case 'X':
                dofIndex = 0;
                SerialUSB.print(" mm ");
                serialInputState = SERIAL_STATE_MOVETO_VALUE;
                break;
            case 'Y':
                dofIndex = 1;
                SerialUSB.print(" mm ");
                serialInputState = SERIAL_STATE_MOVETO_VALUE;
                break;
            case 'Z':
                dofIndex = 2;
                SerialUSB.print(" mm ");
                serialInputState = SERIAL_STATE_MOVETO_VALUE;
                break;
            case 'R':
                dofIndex = 3;
                SerialUSB.print("otate angle ");
                serialInputState = SERIAL_STATE_MOVETO_VALUE;
                break;
            case 'G':
                dofIndex = 4;
                SerialUSB.print("ripper angle ");
                serialInputState = SERIAL_STATE_MOVETO_VALUE;
                break;
            default:
                SerialUSB.println(" Invalid parameter");
                serialInputState = SERIAL_STATE_IDLE;
            }
            break;
        case SERIAL_STATE_K_M_ID:
            switch (rc) {
            case 'J':
            case 'M':
                SerialUSB.println();
                dumpSettings(rc);
                serialInputState = SERIAL_STATE_IDLE;
                break;
            case 0x04: // ^d
                SerialUSB.println();
                defaultSettings();
                serialInputState = SERIAL_STATE_IDLE;
                break;
            case 0x13: // ^s
                SerialUSB.println();
                saveSettings();
                serialInputState = SERIAL_STATE_IDLE;
                break;
            case 0x0C: // ^l
                SerialUSB.println();
                loadSettings();
                serialInputState = SERIAL_STATE_IDLE;
                break;
            case 'S':
                dofIndex = 0;
                SerialUSB.print("houlder ");
                serialInputState = SERIAL_STATE_K_P_ID;
                break;
            case 'E':
                dofIndex = 1;
                SerialUSB.print("lbow ");
                serialInputState = SERIAL_STATE_K_P_ID;
                break;
            case 'W':
                dofIndex = 2;
                SerialUSB.print("rist ");
                serialInputState = SERIAL_STATE_K_P_ID;
                break;
            case 'R':
                dofIndex = 3;
                SerialUSB.print("otate ");
                serialInputState = SERIAL_STATE_K_P_ID;
                break;
            case 'P':
                dofIndex = 4;
                SerialUSB.print("ivot ");
                serialInputState = SERIAL_STATE_K_P_ID;
                break;
            default:
                SerialUSB.println(" Invalid joint");
                serialInputState = SERIAL_STATE_IDLE;
            }
            break;
        case SERIAL_STATE_K_P_ID:
            if (ri < NUM_OF_CONSTS_PER_DOF) {
                serialInputState = SERIAL_STATE_K_VALUE;
                constIndex = ri;
                rxBufIndex = 0;
                SerialUSB.print(" ");
            } else {
                SerialUSB.println(" Invalid constant #");
                serialInputState = SERIAL_STATE_IDLE;
            }
            break;
        case SERIAL_STATE_NEO_COLOR:
            switch (rc) {
            case 'R':
                dofIndex = 0;
                serialInputState = SERIAL_STATE_NEO_VALUE;
                SerialUSB.print("ed ");
                break;
            case 'G':
                dofIndex = 1;
                serialInputState = SERIAL_STATE_NEO_VALUE;
                SerialUSB.print("reen ");
                break;
            case 'B':
                dofIndex = 2;
                serialInputState = SERIAL_STATE_NEO_VALUE;
                SerialUSB.print("lue ");
                break;
            case 'W':
                dofIndex = 3;
                serialInputState = SERIAL_STATE_NEO_VALUE;
                SerialUSB.print("hite ");
                break;
            default:
                serialInputState = SERIAL_STATE_IDLE;
                SerialUSB.println(" Invalid color");
            }
            rxBufIndex = 0;
            break;
        case SERIAL_STATE_K_VALUE:
        case SERIAL_STATE_JOINT_ANGLE:
        case SERIAL_STATE_MOVETO_VALUE:
        case SERIAL_STATE_NEO_VALUE:
        case SERIAL_STATE_VEL_FACTOR:
        case SERIAL_STATE_JOG_ANGLE:
            if ((ri < 10 || rc == '.' || rc == '-') && rxBufIndex < 9) {
                rxBuf[rxBufIndex] = rc;
                rxBufIndex++;
            } else if (rc == 0x08) { // backspace
                rxBufIndex--;
                SerialUSB.print(' ');
                SerialUSB.write(0x08);
            } else if (rc == '\n' || rc == '\r') {
                rxBuf[rxBufIndex] = 0; // NULL-terminated C-string
                SerialUSB.println();
                if (serialInputState == SERIAL_STATE_K_VALUE) {
                    float f = atof(rxBuf);
                    updateSetting(dofIndex, constIndex, f);
                } else if (serialInputState == SERIAL_STATE_JOINT_ANGLE) {
                    float fAng = atof(rxBuf);
                    if (fAng > kJoint[dofIndex].degMax) {
                        SerialUSB.print("Invalid - max = ");
                        SerialUSB.println(kJoint[dofIndex].degMax);
                    } else if (fAng < kJoint[dofIndex].degMin) {
                        SerialUSB.print("Invalid - min = ");
                        SerialUSB.println(kJoint[dofIndex].degMin);
                    } else {
                        fTargetDegrees[dofIndex] = fAng;
                        SerialUSB.print('J');
                        SerialUSB.print(dofIndex);
                        SerialUSB.print(" move to ");
                        SerialUSB.println(fAng);
                    }
                } else if (serialInputState == SERIAL_STATE_JOG_ANGLE) {
                    float fDelta = atof(rxBuf);
                    addJog(dofIndex, fDelta, velocityFactor);
                } else if (serialInputState == SERIAL_STATE_MOVETO_VALUE) {
                    int16_t v = atoi(rxBuf);
                    moveToParams[dofIndex] = v;
                } else if (serialInputState == SERIAL_STATE_NEO_VALUE) {
                    int16_t v = atoi(rxBuf);
                    if (v >= 0 && v < 256) {
                        if (dofIndex == 3) {
                            neoColor[0] = v;
                            neoColor[1] = v;
                            neoColor[2] = v;
                        } else {
                            neoColor[dofIndex] = v;
                        }
                        strip.fill(strip.Color(neoColor[0], neoColor[1], neoColor[2]));
                        strip.show();
                        strip.show();
                    } else
                        SerialUSB.println("Invalid value");
                } else if (serialInputState == SERIAL_STATE_VEL_FACTOR) {
                    int16_t v = atoi(rxBuf);
                    if (v > 0 && v <= 16) {
                        velocityFactor = v;
                    } else
                        SerialUSB.println("Invalid value");
                }
                serialInputState = SERIAL_STATE_IDLE;
            } else {
                SerialUSB.println(" Invalid key - try again");
                serialInputState = SERIAL_STATE_IDLE;
            }
            break;
        }
    }
}

void where()
{
    for (uint8_t i = 0; i < NUM_OF_DOF; i++) {
        SerialUSB.print(jointNames[i]);
        vardump(" targetDeg", fTargetDegrees[i]);
    }
    SerialUSB.println();

    SerialUSB.print("X: ");
    SerialUSB.println(moveToParams[0]);
    SerialUSB.print("Y: ");
    SerialUSB.println(moveToParams[1]);
    SerialUSB.print("Z: ");
    SerialUSB.println(moveToParams[2]);
    SerialUSB.print("R: ");
    SerialUSB.println(moveToParams[3]);
    SerialUSB.print("G: ");
    SerialUSB.println(moveToParams[4]);
    SerialUSB.println();

    for (uint8_t i = 0; i < NUM_OF_MOTORS; i++) {
        SerialUSB.print('M');
        SerialUSB.print(i);
        SerialUSB.print(" currentPosition ");
        SerialUSB.print(accelSteppers[i].currentPosition());
        if (accelSteppers[i].currentPosition() != accelSteppers[i].targetPosition()) {
            SerialUSB.print(" targetPosition ");
            SerialUSB.print(accelSteppers[i].targetPosition());
        }
        SerialUSB.println();
    }
    SerialUSB.println();
}
