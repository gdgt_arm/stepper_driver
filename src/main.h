#ifndef MAIN_H
#define MAIN_H

#include <AccelStepper.h>
#include <Adafruit_NeoPixel.h>
#include <Arduino.h>

#include "DelayTimer.h"
#include "TMCStepper.h"
#include "checkSerial.h"
#include "checkSerialUSB.h"
#include "serial.print.h"
#include "settings.h"
#include "vardump.h"
#include "stepper_controller_driver.h"

#define LED1 1
#define LED0 0
#define LED_ON() digitalWrite(LED_BUILTIN, LED1)
#define LED_OFF() digitalWrite(LED_BUILTIN, LED0)
#define SW_OPEN 1
#define SW_CLOSED 0

#define STATUS_OK 0

#define MOTION_QUEUE_SIZE 16

#define SERBAUD_TMC_DRIVERS 115200

const uint8_t UPPER_ARM_MM = 177; // b = shoulder pivot to elbow pivot in mm
const uint8_t LOWER_ARM_MM = 162; // c = elbow pivot to wrist pivot in mm
const uint8_t GRIPPER_MM = 60 + 49; // g = wrist pivot to gripper center in mm
                                    // 60 mm wrist pivot to front of coupler
                                    // 49 mm coupler mating plate to gripper palm
                                    // 85 mm coupler mating plate to center of can

struct JOINTCONSTS {
    float degMin;
    float degMax;
    float degPark;
    float degUnpark;
    float degExtend;
    float fStepsPerDeg;
    float comp;
};
#define NUM_OF_CONSTS_PER_DOF 7

struct MOTORCONSTS {
    uint8_t microSteps;
    uint16_t currentMax;
    uint16_t currentHold;
    uint16_t maxSpeed;
    uint16_t accel;
};

struct MOTIONTARGET {
    float fDegrees[NUM_OF_DOF];
    uint8_t velocityFactor;
    uint8_t state;
};

struct MOTIONQUEUE {
    union {
        struct MOTIONTARGET move;
        struct JOG_DATA jog;
    };
    enum{MOVE, JOG} type;
};

// struct motorCurState {
//   uint8_t targetDeg;
//   long targetSteps;
//   bool moving;
// };

/********************************************************
   Function Prototypes
********************************************************/

void handleCmd(uint8_t cmd, uint8_t params[] = {});
void moveArm(int8_t motionQueueIndex);
uint8_t moveTo(int16_t mmTX, int16_t mmTY, int16_t mmTZ, float fDegR, uint8_t degG, uint8_t velocityFactor);
void jogMotor(uint8_t motor, int8_t dir);

int8_t addMotionIndex();
int8_t getMotionIndex();
int8_t addMotion(float fAng[], uint8_t velocityFactor, uint8_t state);
int8_t addMotion(float fAng0, float fAng1, float fAng2, float fAng3, float fAng4, uint8_t velocityFactor, uint8_t state);
int8_t addJog(uint8_t joint, float fDelta, uint8_t velocityFactor);
void clearQueue();

void setPos(uint8_t joint, float fAng);
void steps2fTargetDegrees();

#endif
