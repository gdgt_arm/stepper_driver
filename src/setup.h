#ifndef SETUP_H
#define SETUP_H

#include "main.h"
#include <FlashStorage_SAMD.hpp>

extern struct JOINTCONSTS kJoint[NUM_OF_DOF];
extern struct MOTORCONSTS kMotor[NUM_OF_MOTORS];

#endif
