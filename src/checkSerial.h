#ifndef CHECKSERIAL_H
#define CHECKSERIAL_H

#define RX_IDLE 0x00
#define RX_START_RECD 0x01

#define PACKET_START 0x7E
#define PACKET_ESC 0x7D
#define PACKET_XOR 0x20

void checkSerial();
void sizes();
void rxRecvBytes(uint8_t bytes, uint8_t state);
void sendStatus();

#endif