#include "main.h"
#include <Arduino.h>

extern DelayTimer dtBlink;
extern float fTargetDegrees[NUM_OF_DOF];
extern Adafruit_NeoPixel strip;
extern uint8_t velocityFactor;
extern AccelStepper accelSteppers[NUM_OF_DOF];
extern uint8_t armState;
extern bool isMoving;
extern int32_t rotateStepsOffset;

union {
    uint8_t buf[sizeof(MOVE_DATA)]; // MOVE_DATA is the biggest thing we are puttting in rxData
    struct MOVE_DATA move;
    struct MOVETO_DATA moveTo;
    struct JOG_DATA jog;
    struct SETPOS_DATA setPos;
    uint8_t cmd;
    uint8_t neo[3];
} rxData;

uint8_t rxPtr = 0;
uint8_t rxState = RX_IDLE;
uint8_t rxBytes;
bool rxEscRecd;
uint8_t lastErr = 0;

union {
    uint8_t buf[sizeof(STATUS_DATA)]; // STATUS_DATA is the biggest thing we are puttting in txData
    struct STATUS_DATA status;
} txData;

/********************************************************
   Incoming Serial Command Functions
********************************************************/

void checkSerial()
{
    if (Serial.available() > 0) {
        LED_ON();
        dtBlink.reset(0, 10);
        uint8_t rc = Serial.read();
        hexdump("recd", rc);

        if (rc == PACKET_START) {
            rxState = RX_START_RECD;
            return;
        }

        if (rxState == RX_IDLE)
            return; // Still waiting for a start byte

        if (rxState == RX_START_RECD) { // This is the command byte
            if (rc < ARM_CMD_DISABLE) { // Immediate arm command - no parameters
                vardump("ARM_CMD_CMD", rc);
                handleCmd(rc);
                sendStatus();
                rxState = RX_IDLE;
                return;
            }

            switch (rc) {
            case ARM_CMD_DISABLE:
            case ARM_CMD_ENABLE:
                rxRecvBytes(1, rc); // Which motors?
                break;
            case ARM_CMD_STATUS:
                sendStatus();
                rxState = RX_IDLE;
                break;
            case ARM_CMD_MOVE:
                rxRecvBytes(sizeof(MOVE_DATA), ARM_CMD_MOVE);
                break;
            case ARM_CMD_MOVETO:
                rxRecvBytes(sizeof(MOVETO_DATA), ARM_CMD_MOVETO);
                break;
            case ARM_CMD_JOG:
                rxRecvBytes(sizeof(JOG_DATA), ARM_CMD_JOG); // Which joint and how far?
                break;
            case ARM_CMD_SETPOS:
                rxRecvBytes(sizeof(SETPOS_DATA), ARM_CMD_SETPOS); // Which joint and what angle?
                break;
            case ARM_CMD_LED:
                rxRecvBytes(1, ARM_CMD_LED); // On or off?
                break;
            case ARM_CMD_NEO:
                rxRecvBytes(3, ARM_CMD_NEO); // What RGB values?
                break;
            }
            return;
        }

        if (rc == PACKET_ESC) {
            rxEscRecd = true;
            return;
        }

        if (rxEscRecd) {
            rc = rc ^ PACKET_XOR;
            rxEscRecd = false;
        }

        // All other states
        rxData.buf[rxPtr] = rc;
        rxPtr++;
        if (rxPtr == rxBytes) {
            int8_t lastAddMotionIndex = 0;

            switch (rxState) {
            case ARM_CMD_DISABLE:
            case ARM_CMD_ENABLE:
                handleCmd(rxState, rxData.buf);
                break;
            case ARM_CMD_MOVE:
                serprintln("ARM_CMD_MOVE");
                lastAddMotionIndex = addMotion(
                    rxData.move.fAng,
                    rxData.move.velocityFactor,
                    ARM_STATE_EXTENDED);
                lastErr = lastAddMotionIndex == -1 ? ARM_ERR_QUEUE_FULL : ARM_ERR_NONE;
                break;
            case ARM_CMD_MOVETO:
                serprintln("ARM_CMD_MOVETO");
                lastErr = moveTo(
                    rxData.moveTo.x,
                    rxData.moveTo.y,
                    rxData.moveTo.z,
                    rxData.moveTo.angR,
                    rxData.moveTo.angG,
                    rxData.moveTo.velocityFactor);
                break;
            case ARM_CMD_JOG:
                serprintln("ARM_CMD_JOG");
                lastAddMotionIndex = addJog(
                    rxData.jog.joint,
                    rxData.jog.fDelta,
                    rxData.jog.velocityFactor);
                lastErr = lastAddMotionIndex == -1 ? ARM_ERR_QUEUE_FULL : ARM_ERR_NONE;
                break;
            case ARM_CMD_SETPOS:
                serprintln("ARM_CMD_SETPOS");
                setPos(
                    rxData.setPos.joint,
                    rxData.setPos.fAng);
                break;
            case ARM_CMD_LED:
                serprintln("ARM_CMD_LED");
                if (rxData.cmd) {
                    LED_ON();
                    dtBlink.stop();
                } else
                    dtBlink.setDelay(1);
                break;
            case ARM_CMD_NEO:
                serprintln("ARM_CMD_NEO");
                strip.fill(strip.Color(rxData.neo[0], rxData.neo[1], rxData.neo[2]));
                strip.show();
                strip.show();
                break;
            }
            sendStatus();
            rxState = RX_IDLE;
        }
    }
}

void rxRecvBytes(uint8_t bytes, uint8_t state)
{
    rxPtr = 0;
    rxBytes = bytes;
    rxEscRecd = false;
    rxState = state;
}

void sendStatus()
{
    serprintln("send status");
    if (isMoving) {
        txData.status.fAng[JOINT_SHOULDER] = (float)accelSteppers[MOTOR_SHOULDER].currentPosition() / kJoint[JOINT_SHOULDER].fStepsPerDeg;

        txData.status.fAng[JOINT_ELBOW] = (float)accelSteppers[MOTOR_ELBOW].currentPosition() / kJoint[JOINT_ELBOW].fStepsPerDeg;
        txData.status.fAng[JOINT_ELBOW] -= (float)txData.status.fAng[JOINT_SHOULDER] * kJoint[JOINT_ELBOW].comp;

        txData.status.fAng[JOINT_WRIST] = (float)(accelSteppers[MOTOR_WRIST0].currentPosition() - rotateStepsOffset) / kJoint[JOINT_WRIST].fStepsPerDeg;
        txData.status.fAng[JOINT_WRIST] -= ((float)txData.status.fAng[JOINT_SHOULDER] * kJoint[JOINT_ELBOW].comp + (float)txData.status.fAng[JOINT_ELBOW]) * kJoint[JOINT_WRIST].comp - 0.5;

        txData.status.fAng[JOINT_ROTATE] = (float)rotateStepsOffset / kJoint[JOINT_ROTATE].fStepsPerDeg;

        txData.status.fAng[JOINT_PIVOT] = (float)accelSteppers[MOTOR_PIVOT].currentPosition() / kJoint[JOINT_PIVOT].fStepsPerDeg;
    } else {
        for (uint8_t i = 0; i < NUM_OF_DOF; i++) {
            txData.status.fAng[i] = fTargetDegrees[i];
        }
    }
    txData.status.state = armState;
    txData.status.moving = isMoving;
    txData.status.err = lastErr;

    // Send it
    Serial.write(PACKET_START);
    for (uint8_t i = 0; i < sizeof(STATUS_DATA); i++) {
        if (txData.buf[i] == PACKET_START || txData.buf[i] == PACKET_ESC) {
            Serial.write(PACKET_ESC);
            Serial.write(txData.buf[i] ^ PACKET_XOR);
        } else {
            Serial.write(txData.buf[i]);
        }
    }
}

void sizes()
{
    vardump("MOVE_DATA", (uint8_t)sizeof(MOVE_DATA));
    vardump("MOVETO_DATA", (uint8_t)sizeof(MOVETO_DATA));
    vardump("SETPOS_DATA", (uint8_t)sizeof(SETPOS_DATA));
}
