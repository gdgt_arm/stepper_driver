#include "main.h"

#include "setup.h"

/********************************************************
   Global Variables
********************************************************/

uint8_t errnum = STATUS_OK;

Adafruit_NeoPixel strip(1, PIN_NEO, NEO_GRB + NEO_KHZ800);
uint8_t neoColor[3] = { 0, 0, 0 };

TMC2209Stepper tmcSteppers[NUM_OF_DOF] = {
    TMC2209Stepper(&Serial1, 0.15f, 0b00),
    TMC2209Stepper(&Serial1, 0.15f, 0b01),
    TMC2209Stepper(&Serial1, 0.15f, 0b10),
    TMC2209Stepper(&Serial2, 0.15f, 0b00),
    TMC2209Stepper(&Serial2, 0.15f, 0b01)
};

AccelStepper accelSteppers[NUM_OF_DOF] = {
    AccelStepper(PIN_STEP_1, PIN_DIR_1, PIN_EN_1),
    AccelStepper(PIN_STEP_2, PIN_DIR_2, PIN_EN_2),
    AccelStepper(PIN_STEP_3, PIN_DIR_3, PIN_EN_3),
    AccelStepper(PIN_STEP_4, PIN_DIR_4, PIN_EN_4),
    AccelStepper(PIN_STEP_5, PIN_DIR_5, PIN_EN_5)
};

DelayTimer dtBlink;
// DelayTimer dtCurrent(500);

char buf[100];
struct JOINTCONSTS kJoint[NUM_OF_DOF];
struct MOTORCONSTS kMotor[NUM_OF_MOTORS];
// struct motorCurState joints[NUM_OF_DOF];
float fTargetDegrees[NUM_OF_DOF];
struct MOTIONQUEUE motionQueue[MOTION_QUEUE_SIZE];
int8_t motionQueueFront = -1;
int8_t motionQueueRear = -1;
int8_t lastMotionQueueIndex = -1;
int32_t rotateStepsOffset = 0;
const char jointNames[NUM_OF_DOF] = { 'S', 'E', 'W', 'R', 'P' };
bool isMoving;

uint8_t armState = ARM_STATE_UNKOWN;

uint8_t velocityFactor = 1;

/********************************************************
   Setup
********************************************************/

void setup()
{
    // Give us something pretty to look at.
    pinMode(LED_BUILTIN, OUTPUT); // Set Heartbeat LED as output
    LED_ON(); // Turn LED on

    strip.begin(); // INITIALIZE NeoPixel strip object (REQUIRED)
    strip.show(); // Turn OFF all pixels ASAP

    // Open a serial connection and wait for a keypress.
    serbeg(SERBAUD_USB);
    int msWait = millis();
    serprintln(F("\nWaiting for MOTU..."));
    while (!seravail() && millis() - msWait < 2000)
        ;
    serprintln(F("\nStarting..."));

    Serial.begin(SERBAUD_CONTROLLER_DRIVER);
    Serial1.begin(SERBAUD_TMC_DRIVERS);
    Serial2.begin(SERBAUD_TMC_DRIVERS);

    errnum = loadSettings(); // Read constants stored in EEPROM
    if (errnum)
        return;

    for (uint8_t i = 0; i < NUM_OF_MOTORS; i++) {
        tmcSteppers[i].begin(); // UART: Init SW UART (if selected) with default 115200 baudrate
        tmcSteppers[i].toff(5); // Enables driver in software
        tmcSteppers[i].rms_current(kMotor[i].currentMax); // Set motor RMS current
        tmcSteppers[i].microsteps(kMotor[i].microSteps); // Set microsteps

        tmcSteppers[i].en_spreadCycle(false); // Toggle spreadCycle on/off
        tmcSteppers[i].pwm_autoscale(true); // Needed for stealthChop
        tmcSteppers[i].SGTHRS(255);
        tmcSteppers[i].GCONF(0b0011000000);
    }
    // */

    // LED_OFF();
    dtBlink.setDelay(1);
    strip.show(); // Turn OFF all pixels ASAP
}

/********************************************************
   Loop
********************************************************/

void loop()
{
    uint32_t msNow = millis();

    if (errnum) {
        if (dtBlink.tripped(msNow)) {
            digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
            dtBlink.reset(msNow, 200);
        }
        return;
    }

    checkSerial();
    checkSerialUSB();

    isMoving = false;
    for (uint8_t i = 0; i < NUM_OF_MOTORS; i++) {
        if (accelSteppers[i].run()) {
            isMoving = true;
        }
    }

    if (!isMoving) {
        int8_t idx = getMotionIndex();
        if (idx > -1)
            moveArm(idx);
    }

    if (dtBlink.tripped(msNow)) {
        if (digitalRead(LED_BUILTIN) == LED1) {
            LED_OFF();
            dtBlink.reset(msNow, 1990);
        } else {
            LED_ON();
            dtBlink.reset(msNow, 10);
        }
    }

    // if (dtCurrent.tripped(msNow)) {
    //     // uint16_t i = tmcSteppers[4].SG_RESULT();
    //     SerialUSB.print("0 ");
    //     SerialUSB.print(tmcSteppers[4].SG_RESULT(), DEC);
    //     SerialUSB.print(" ");
    //     SerialUSB.println(tmcSteppers[4].cs2rms(tmcSteppers[4].cs_actual()), DEC);
    // }
}

/********************************************************
   Arm Functions
********************************************************/

void handleCmd(uint8_t cmd, uint8_t params[])
{
    switch (cmd) {
    case ARM_CMD_STOP:
    case ARM_CMD_STOP_DECEL: // TODO add ARM_CMD_STOP_DECEL version that  decelerates arm motion
        for (uint8_t i = 0; i < NUM_OF_MOTORS; i++) {
            accelSteppers[i].stop();
        }

        // steps2fTargetDegrees();
        armState = ARM_STATE_EXTENDED;
        clearQueue();
        break;
    case ARM_CMD_LIMP:
        for (uint8_t i = 0; i < NUM_OF_MOTORS; i++) {
            accelSteppers[i].enable(false);
        }
        for (uint8_t i = 0; i < NUM_OF_DOF; i++) {
            fTargetDegrees[i] = 0;
        }
        armState = ARM_STATE_UNKOWN;
        clearQueue();
        break;
    case ARM_CMD_INIT: {
        long targetSteps[NUM_OF_MOTORS];
        float fTargetDeg;

        for (uint8_t i = 0; i < NUM_OF_MOTORS; i++) {
            tmcSteppers[i].begin(); // UART: Init SW UART (if selected) with default 115200 baudrate
            tmcSteppers[i].toff(5); // Enables driver in software
            tmcSteppers[i].rms_current(kMotor[i].currentMax); // Set motor RMS current
            tmcSteppers[i].microsteps(kMotor[i].microSteps); // Set microsteps

            tmcSteppers[i].en_spreadCycle(false); // Toggle spreadCycle on/off
            tmcSteppers[i].pwm_autoscale(true); // Needed for stealthChop
            tmcSteppers[i].GCONF(0b0011000000);
        }

        fTargetDegrees[JOINT_ROTATE] = kJoint[JOINT_ROTATE].degPark;
        rotateStepsOffset = round(fTargetDegrees[JOINT_ROTATE] * kJoint[JOINT_ROTATE].fStepsPerDeg);

        for (uint8_t i = 0; i < NUM_OF_DOF; i++) {
            fTargetDeg = fTargetDegrees[i] = kJoint[i].degPark;
            serprint(jointNames[i]);
            vardump(" INIT fTargetDeg", fTargetDeg);
            if (i == JOINT_ROTATE)
                continue;
            if (i == JOINT_ELBOW)
                fTargetDeg += kJoint[JOINT_SHOULDER].degPark * kJoint[JOINT_ELBOW].comp;
            else if (i == JOINT_WRIST)
                fTargetDeg += (kJoint[JOINT_SHOULDER].degPark * kJoint[JOINT_ELBOW].comp + kJoint[JOINT_ELBOW].degPark) * kJoint[JOINT_WRIST].comp;

            long steps = round(fTargetDeg * kJoint[i].fStepsPerDeg);
            if (i == JOINT_WRIST) {
                targetSteps[MOTOR_WRIST0] = steps + rotateStepsOffset;
                targetSteps[MOTOR_WRIST1] = steps - rotateStepsOffset;
            } else {
                targetSteps[i] = steps;
            }
        }

        for (uint8_t i = 0; i < NUM_OF_MOTORS; i++) {
            accelSteppers[i].enable(true);
            accelSteppers[i].setCurrentPosition(targetSteps[i]);
        }

        armState = ARM_STATE_PARKED;
        clearQueue();
        break;
    }
    case ARM_CMD_PARK:
        switch (armState) {
        case ARM_STATE_UNKOWN:
            serprintln("Init first");
            break;
        case ARM_STATE_PARKED:
            break;
        case ARM_STATE_EXTENDED:
            serprintln("unpark");
            handleCmd(ARM_CMD_UNPARK);
        default:
            addMotion(
                kJoint[0].degPark,
                kJoint[1].degPark,
                kJoint[2].degPark,
                kJoint[3].degPark,
                kJoint[4].degPark,
                velocityFactor,
                ARM_STATE_PARKED);
        }
        break;
    case ARM_CMD_UNPARK:
        switch (armState) {
        case ARM_STATE_UNKOWN:
            serprintln("Init first");
            break;
        case ARM_STATE_UNPARKED:
            break;
        case ARM_STATE_EXTENDED:
            // if (joints[ARM_E_MOTOR].target > 90) {
            //     moveArmMotor(ARM_E_MOTOR, 90, 0);
            //     delay(200);
            // }
            // if (joints[ARM_W_MOTOR].target < 180) {
            //     moveArmMotor(ARM_W_MOTOR, 180, 0);
            //     delay(200);
            // }
        default:
            addMotion(
                kJoint[0].degUnpark,
                kJoint[1].degUnpark,
                kJoint[2].degUnpark,
                kJoint[3].degUnpark,
                kJoint[4].degUnpark,
                velocityFactor,
                ARM_STATE_UNPARKED);
        }
        break;
    case ARM_CMD_EXTEND:
        switch (armState) {
        case ARM_STATE_UNKOWN:
            serprintln("Init first");
            break;
        case ARM_STATE_PARKED:
            serprintln("unpark");
            handleCmd(ARM_CMD_UNPARK);
        default:
            addMotion(
                kJoint[0].degExtend,
                kJoint[1].degExtend,
                kJoint[2].degExtend,
                kJoint[3].degExtend,
                kJoint[4].degExtend,
                velocityFactor,
                ARM_STATE_EXTENDED);
        }
        break;
    case ARM_CMD_MOVE:
        switch (armState) {
        case ARM_STATE_UNKOWN:
            serprintln("Init first");
            break;
        case ARM_STATE_PARKED: {
            serprintln("unpark");
            handleCmd(ARM_CMD_UNPARK);
        }
        default:
            addMotion(
                fTargetDegrees[0],
                fTargetDegrees[1],
                fTargetDegrees[2],
                fTargetDegrees[3],
                fTargetDegrees[4],
                velocityFactor,
                ARM_STATE_EXTENDED);
        }
        break;
    // case ARM_CMD_DANCE:
    //     switch (armState) {
    //     case ARM_STATE_UNKOWN:
    //         serprintln("Init first");
    //         break;
    //     case ARM_STATE_PARKED:
    //         handleCmd(ARM_CMD_UNPARK);
    //     default:
    //         handleCmd(ARM_CMD_EXTEND);
    //         // high up
    //         addMotion(145, 180, 0, 0, 0, 1, ARM_STATE_EXTENDED);
    //         // elbow bent over
    //         addMotion(160, 45, 0, 45, 0, 1, ARM_STATE_EXTENDED);
    //         // out at 45°
    //         addMotion(135, 180, 0, -45, 0, 1, ARM_STATE_EXTENDED);
    //         // unpark
    //         handleCmd(ARM_CMD_UNPARK);
    //     }
    //     break;
    case ARM_CMD_TOOL_READY:
        switch (armState) {
        case ARM_STATE_UNKOWN:
            serprintln("Init first");
            break;
        case ARM_STATE_PARKED:
            handleCmd(ARM_CMD_UNPARK);
        default:
            addMotion(133, 38, 90, 0, 0, 1, ARM_STATE_TOOL_READY);
        }
        break;
    case ARM_CMD_TOOL_GET:
        handleCmd(ARM_CMD_TOOL_READY);
        addMotion(132, 51, 81, 0, 0, 1, ARM_STATE_TOOL_READY);
        break;
    case ARM_CMD_TOOL_LOCK:
        handleCmd(ARM_CMD_TOOL_GET);
        addMotion(136, 51, 84, 20, 0, 1, ARM_STATE_EXTENDED);
        addMotion(150, 50, 73, 20, 0, 1, ARM_STATE_EXTENDED);
        break;
    case ARM_CMD_TOOL_RETURN:
        switch (armState) {
        case ARM_STATE_EXTENDED:
            addMotion(150, 50, 73, 20, 0, 1, ARM_STATE_EXTENDED);
            addMotion(136, 51, 84, 20, 0, 1, ARM_STATE_EXTENDED);
            addMotion(136, 51, 83, 0, 0, 1, ARM_STATE_TOOL_READY);
            addMotion(137, 44, 90, 0, 0, 1, ARM_STATE_TOOL_READY);
            handleCmd(ARM_CMD_UNPARK);
        }
        break;
    case ARM_CMD_DISABLE:
        serprintln("ARM_CMD_DISABLE");
        for (uint8_t i = 0; i < NUM_OF_MOTORS; i++) {
            if (params[0] & (1 << i))
                accelSteppers[i].enable(false);
        }
        armState = ARM_STATE_UNKOWN;
        clearQueue();
        break;
    case ARM_CMD_ENABLE:
        serprintln("ARM_CMD_ENABLE");
        for (uint8_t i = 0; i < NUM_OF_MOTORS; i++) {
            if (params[0] & (1 << i))
                accelSteppers[i].enable(true);
        }
        armState = ARM_STATE_UNKOWN;
        clearQueue();
        break;
    }
}

uint8_t moveTo(int16_t mmTX, int16_t mmTY, int16_t mmTZ, float fDegR, uint8_t degG, uint8_t velocityFactor)
{
    vardump("mmTX hori", mmTX);
    vardump("mmTY vert", mmTY);
    vardump("mmTZ dist", mmTZ);
    vardump("fDegR wrist rotate", fDegR);
    vardump("degG grab angle 90 = hori", degG);
    serprintln();

    float fmmTP = sqrt(mmTX * mmTX + mmTZ * mmTZ);
    vardump("fmmTP", fmmTP);

    // float t = (float) mmTX / mmTY;
    // vardump("t", t);
    // float radP = atan(mmTX / mmTY);
    // vardump("radP", radP);
    float fDegP = degrees((atan((float)mmTX / (float)mmTZ)));
    vardump("fDegP", fDegP);

    if (fDegP < kJoint[JOINT_PIVOT].degMin || fDegP > kJoint[JOINT_PIVOT].degMax)
        return ARM_ERR_P_ANGLE;

    vardump("GRIPPER_MM", GRIPPER_MM);

    float fmmGP = sin(radians(degG)) * GRIPPER_MM;
    vardump("fmmGP", fmmGP);
    float fmmGY = cos(radians(degG)) * GRIPPER_MM;
    vardump("fmmGY", fmmGY);

    // subtract g from t to get a
    float fmmAP = fmmTP - fmmGP;
    vardump("fmmAP", fmmAP);
    float fmmAY = (float)mmTY + fmmGY;
    vardump("fmmAY", fmmAY);

    // TODO Test ax, ay are within range (in front of body)
    if (false)
        return ARM_ERR_NOT_FRONT;

    float fmmA = sqrt(fmmAP * fmmAP + fmmAY * fmmAY);
    vardump("fmmA", fmmA);

    vardump("UPPER_ARM_MM", UPPER_ARM_MM);
    vardump("LOWER_ARM_MM", LOWER_ARM_MM);

    // If a > b + c abort
    if (fmmA > (UPPER_ARM_MM + LOWER_ARM_MM))
        return ARM_ERR_TOO_FAR;

    float fRadX = PI - acos(fmmAY / fmmA);
    vardump("fRadX", fRadX);
    vardump("degX", degrees(fRadX));
    float fRadC = acos((fmmA * fmmA + UPPER_ARM_MM * UPPER_ARM_MM - LOWER_ARM_MM * LOWER_ARM_MM) / (2.0 * fmmA * UPPER_ARM_MM));
    vardump("fRadC", fRadC);
    vardump("degC", degrees(fRadC));
    float fDegS = degrees((fRadX + fRadC));
    vardump("fDegS", fDegS);

    // Test S is within limits
    if (fDegS < kJoint[JOINT_SHOULDER].degMin || fDegS > kJoint[JOINT_SHOULDER].degMax)
        return ARM_ERR_S_ANGLE;

    float fDegE = degrees(acos(((float)UPPER_ARM_MM * UPPER_ARM_MM + LOWER_ARM_MM * LOWER_ARM_MM - fmmA * fmmA) / (2.0 * UPPER_ARM_MM * LOWER_ARM_MM)));
    vardump("fDegE", fDegE);

    // Test E is within limits
    if (fDegE < kJoint[JOINT_ELBOW].degMin || fDegE > kJoint[JOINT_ELBOW].degMax)
        return ARM_ERR_E_ANGLE;

    float fDegW = (float)degG - fDegS - fDegE + 180.0;
    vardump("fDegW", fDegW);

    // Test W is within limits
    if (fDegW < kJoint[JOINT_WRIST].degMin || fDegW > kJoint[JOINT_WRIST].degMax)
        return ARM_ERR_W_ANGLE;

    if (addMotion(fDegS, fDegE, fDegW, fDegR, fDegP, velocityFactor, ARM_STATE_EXTENDED) == -1)
        return ARM_ERR_QUEUE_FULL;

    return ARM_ERR_NONE;
}

void moveArm(int8_t motionQueueIndex)
{
    long targetSteps[NUM_OF_MOTORS];
    long delta[NUM_OF_MOTORS];
    long steps;
    float fLongestTravel = 0.0;
    float fTargetDeg;
    uint8_t velocityFactor = 1;

    switch (motionQueue[motionQueueIndex].type) {
    case MOTIONQUEUE::JOG:
        // if(motionQueue[motionQueueIndex].jog.joint == JOINT_ROTATE)
        //     rotateStepsOffset = round(motionQueue[motionQueueIndex].move.fDegrees[JOINT_ROTATE] * kJoint[JOINT_ROTATE].fStepsPerDeg);

        for (uint8_t i = 0; i < NUM_OF_MOTORS; i++) {
            targetSteps[i] = accelSteppers[i].currentPosition();
        }
        steps = round(motionQueue[motionQueueIndex].jog.fDelta * kJoint[motionQueue[motionQueueIndex].jog.joint].fStepsPerDeg);
        if (motionQueue[motionQueueIndex].jog.joint == JOINT_WRIST) {
            targetSteps[MOTOR_WRIST0] += steps;
            targetSteps[MOTOR_WRIST1] += steps;
        } else if (motionQueue[motionQueueIndex].jog.joint == JOINT_ROTATE) {
            targetSteps[MOTOR_WRIST0] += steps;
            targetSteps[MOTOR_WRIST1] -= steps;
        } else {
            targetSteps[motionQueue[motionQueueIndex].jog.joint] += steps;
        }
        velocityFactor = motionQueue[motionQueueIndex].jog.velocityFactor;
        break;
    case MOTIONQUEUE::MOVE:
        rotateStepsOffset = round(motionQueue[motionQueueIndex].move.fDegrees[JOINT_ROTATE] * kJoint[JOINT_ROTATE].fStepsPerDeg);

        for (uint8_t i = 0; i < NUM_OF_DOF; i++) {
            fTargetDeg = fTargetDegrees[i] = motionQueue[motionQueueIndex].move.fDegrees[i];
            serprint(jointNames[i]);
            vardump(" MOVEARM fTargetDeg", fTargetDeg);
            if (i == JOINT_ROTATE)
                continue;
            if (i == JOINT_ELBOW)
                fTargetDeg += (float)motionQueue[motionQueueIndex].move.fDegrees[JOINT_SHOULDER] * kJoint[JOINT_ELBOW].comp;
            else if (i == JOINT_WRIST)
                fTargetDeg += ((float)motionQueue[motionQueueIndex].move.fDegrees[JOINT_SHOULDER] * kJoint[JOINT_ELBOW].comp + (float)motionQueue[motionQueueIndex].move.fDegrees[JOINT_ELBOW]) * kJoint[JOINT_WRIST].comp;
            steps = round(fTargetDeg * kJoint[i].fStepsPerDeg);
            if (i == JOINT_WRIST) {
                targetSteps[MOTOR_WRIST0] = steps + rotateStepsOffset;
                targetSteps[MOTOR_WRIST1] = steps - rotateStepsOffset;
            } else {
                targetSteps[i] = steps;
            }
        }
        velocityFactor = motionQueue[motionQueueIndex].move.velocityFactor;
        armState = motionQueue[motionQueueIndex].move.state;
        break;
    }

    bool needToMove = false;
    for (uint8_t i = 0; i < NUM_OF_MOTORS; i++) {
        delta[i] = abs(targetSteps[i] - accelSteppers[i].currentPosition());
        if (delta[i]) {
            needToMove = true;
            float fTravel = (float)delta[i] / (float)kMotor[i].maxSpeed;
            if (fTravel > fLongestTravel)
                fLongestTravel = fTravel;
        }
    }
    // serprint(F("fLongestTravel: "));
    // serprintln(fLongestTravel);
    if (needToMove) {
        for (uint8_t i = 0; i < NUM_OF_MOTORS; i++) {
            float speed = (float)delta[i] / fLongestTravel / velocityFactor;
            accelSteppers[i].setMaxSpeed(speed);
            accelSteppers[i].setAcceleration(speed * 4);
            accelSteppers[i].moveTo(targetSteps[i]);

            serprint('M');
            serprint(i);
            serprint(F(" moving "));
            serprint(delta[i]);
            serprint(F(" steps to "));
            serprint(targetSteps[i]);
            serprint(F(" @ "));
            serprintln(speed);
        }
    }

    lastMotionQueueIndex = motionQueueIndex;
}

void jogMotor(uint8_t motor, int8_t dir)
{
    char c = 8; // backspace
    serprint(c);
    serprint("jogMotor: ");
    serprint(jointNames[motor]);
    serprint(' ');
    c = dir == 1 ? '+' : '-';
    serprintln(c);

    accelSteppers[motor].setMaxSpeed(kMotor[motor].maxSpeed / 4);
    accelSteppers[motor].move(200 * dir);
}

void setPos(uint8_t joint, float fAng)
{
    long targetSteps[NUM_OF_MOTORS];
    float fTargetDeg;

    fTargetDegrees[joint] = fAng;

    rotateStepsOffset = round(fTargetDegrees[JOINT_ROTATE] * kJoint[JOINT_ROTATE].fStepsPerDeg);

    for (uint8_t i = 0; i < NUM_OF_DOF; i++) {
        fTargetDeg = fTargetDegrees[i];
        serprint(jointNames[i]);
        serprint('=');
        vardump(" SETPOS fTargetDeg", fTargetDeg);
        if (i == JOINT_ROTATE)
            continue;
        if (i == JOINT_ELBOW)
            fTargetDeg += fTargetDegrees[JOINT_SHOULDER] * kJoint[JOINT_ELBOW].comp;
        else if (i == JOINT_WRIST)
            fTargetDeg += (fTargetDegrees[JOINT_SHOULDER] * kJoint[JOINT_ELBOW].comp + fTargetDegrees[JOINT_ELBOW]) * kJoint[JOINT_WRIST].comp;

        if (i == JOINT_WRIST) {
            targetSteps[MOTOR_WRIST0] = round(fTargetDeg * kJoint[JOINT_WRIST].fStepsPerDeg) + rotateStepsOffset;
            targetSteps[MOTOR_WRIST1] = round(fTargetDeg * kJoint[JOINT_WRIST].fStepsPerDeg) - rotateStepsOffset;
        } else {
            targetSteps[i] = round(fTargetDeg * kJoint[i].fStepsPerDeg);
        }
    }

    for (uint8_t i = 0; i < NUM_OF_MOTORS; i++) {
        accelSteppers[i].setCurrentPosition(targetSteps[i]);
    }
}

void steps2fTargetDegrees()
{
    fTargetDegrees[JOINT_SHOULDER] = (float)accelSteppers[MOTOR_SHOULDER].currentPosition() / kJoint[JOINT_SHOULDER].fStepsPerDeg;

    fTargetDegrees[JOINT_ELBOW] = (float)accelSteppers[MOTOR_ELBOW].currentPosition() / kJoint[JOINT_ELBOW].fStepsPerDeg;
    fTargetDegrees[JOINT_ELBOW] -= (float)fTargetDegrees[JOINT_SHOULDER] * kJoint[JOINT_ELBOW].comp;

    fTargetDegrees[JOINT_WRIST] = (float)(accelSteppers[MOTOR_WRIST0].currentPosition() - rotateStepsOffset) / kJoint[JOINT_WRIST].fStepsPerDeg;
    fTargetDegrees[JOINT_WRIST] -= ((float)fTargetDegrees[JOINT_SHOULDER] * kJoint[JOINT_ELBOW].comp + (float)fTargetDegrees[JOINT_ELBOW]) * kJoint[JOINT_WRIST].comp - 0.5;

    fTargetDegrees[JOINT_ROTATE] = (float)rotateStepsOffset / kJoint[JOINT_ROTATE].fStepsPerDeg;

    fTargetDegrees[JOINT_PIVOT] = (float)accelSteppers[MOTOR_PIVOT].currentPosition() / kJoint[JOINT_PIVOT].fStepsPerDeg;
}

/********************************************************
   Queue Functions
********************************************************/

int8_t addMotionIndex()
{
    if (motionQueueFront == ((motionQueueRear + 1) % MOTION_QUEUE_SIZE))
        return -1; // Queue is full!
    if (motionQueueFront == -1)
        motionQueueFront = 0; // Queue is empty
    motionQueueRear = (motionQueueRear + 1) % MOTION_QUEUE_SIZE;
    serprint(F("addMotionIndex: "));
    serprintln(motionQueueRear);
    return motionQueueRear;
}

int8_t getMotionIndex()
{
    if (motionQueueFront == -1)
        return -1; // Queue is empty
    int8_t frontIndex = motionQueueFront;
    if (motionQueueFront == motionQueueRear) {
        motionQueueFront = -1; // Queue now empty
        motionQueueRear = -1;
    } else {
        motionQueueFront = (motionQueueFront + 1) % MOTION_QUEUE_SIZE;
    }
    serprint(F("getMotionIndex: "));
    serprintln(frontIndex);
    return frontIndex;
}

int8_t addMotion(float fAng[], uint8_t velocityFactor, uint8_t state)
{
    return addMotion(fAng[0], fAng[1], fAng[2], fAng[3], fAng[4], velocityFactor, state);
}

int8_t addMotion(float fAng0, float fAng1, float fAng2, float fAng3, float fAng4, uint8_t velocityFactor, uint8_t state)
{
    uint8_t motionQueueIndex = addMotionIndex();
    if (motionQueueIndex > -1) {
        motionQueue[motionQueueIndex].move.fDegrees[0] = fAng0;
        motionQueue[motionQueueIndex].move.fDegrees[1] = fAng1;
        motionQueue[motionQueueIndex].move.fDegrees[2] = fAng2;
        motionQueue[motionQueueIndex].move.fDegrees[3] = fAng3;
        motionQueue[motionQueueIndex].move.fDegrees[4] = fAng4;
        motionQueue[motionQueueIndex].move.velocityFactor = velocityFactor;
        motionQueue[motionQueueIndex].move.state = state;
        motionQueue[motionQueueIndex].type = MOTIONQUEUE::MOVE;
        vardump("\naddMotion S", fAng0);
        vardump("addMotion E", fAng1);
        vardump("addMotion W", fAng2);
        vardump("addMotion R", fAng3);
        vardump("addMotion P", fAng4);
        serprintln();
    }
    return motionQueueIndex;
}

int8_t addJog(uint8_t joint, float fDelta, uint8_t velocityFactor)
{
    uint8_t motionQueueIndex = addMotionIndex();
    if (motionQueueIndex > -1) {
        motionQueue[motionQueueIndex].jog.fDelta = fDelta;
        motionQueue[motionQueueIndex].jog.joint = joint;
        motionQueue[motionQueueIndex].jog.velocityFactor = velocityFactor;
        motionQueue[motionQueueIndex].type = MOTIONQUEUE::JOG;
        vardump("\naddJog joint", joint);
        vardump("addJog fDelta", fDelta);
        vardump("addJog velocityFactor", velocityFactor);
        serprintln();
    }
    return motionQueueIndex;
}

void clearQueue()
{
    motionQueueFront = -1; // Queue now empty
    motionQueueRear = -1;
}
