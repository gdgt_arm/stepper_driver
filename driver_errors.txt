1 ARM_ERR_NOT_FRONT (not tested)
2 ARM_ERR_TOO_FAR = out of reach (too far)
3 ARM_ERR_P_ANGLE = too far left or right
4 ARM_ERR_S_ANGLE = shoulder out of range
5 ARM_ERR_E_ANGLE = elbow out of range
6 ARM_ERR_W_ANGLE = wrist out of range
7 ARM_ERR_QUEUE_FULL = queue full
